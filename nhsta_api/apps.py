from django.apps import AppConfig


class NhstaApiConfig(AppConfig):
    name = 'nhsta_api'
