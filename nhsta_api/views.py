
import requests,json;
from django.shortcuts import render
from django.http import HttpResponse
import json

baseURL= 'https://vpic.nhtsa.dot.gov/api/'
# baseURL= 'http://api.carmd.com/v3.0/'
# baseURL= 'https://vindecoder.p.rapidapi.com/'

def get_all_car_makes(request):
    url = baseURL+'/vehicles//GetAllManufacturers?format=json';
    r = requests.get(url);
    print(r.text);
    return HttpResponse(r)

def decode_vin(request):
    url = "https://vindecoder.p.rapidapi.com/decode_vin"

    querystring = {"vin": "4F2YU09161KM33122"}

    headers = {
        'x-rapidapi-host': "vindecoder.p.rapidapi.com",
        'x-rapidapi-key': "18ae2690bcmshc79b45626fd514dp1e6e03jsn8c9a9f051e4b"
    }

    r = requests.request("GET", url, headers=headers, params=querystring)

    return HttpResponse(json.dumps({"results": r}), status=200)


def get_car_details_from_vin(request):
    url = baseURL+'vehicles/DecodeVINValuesBatch/';
    vin = request.GET['vin']
    if vin:
        post_fields = {'format': 'json', 'data': vin};
    else:
        post_fields = {'format': 'json', 'data': 'WBXHT3C30G5E50557'};

    # post_fields = {'format': 'json', 'data':'13GNDA13D76S000000;5XYKT3A12CG000000'};
    r = requests.post(url, data=post_fields);
    document = json.loads(r.content)['Results'][0]
    results = {
        "Make": document['Make'],
        "Manufacturer": document['Manufacturer'],
        "Model": document['Model'],
        "ModelYear": document['ModelYear'],
        "Series": document['Series'],
        "Trim": document['Trim'],
        "VIN": document['VIN'],
        "VehicleType": document['VehicleType'],
        "PlantCountry": document['PlantCountry'],
        "EngineCylinders": document['EngineCylinders'],
        "EngineHP": document['EngineHP'],
        "EngineKW": document['EngineKW'],
        "FuelTypePrimary": document['FuelTypePrimary'],
        "SeatBeltsAll": document['SeatBeltsAll'],
    }
    print(results)

    return HttpResponse(json.dumps({"results": results}), status=200)


def get_models_from_make(request, make):
    url = baseURL+ 'vehicles/GetModelsForMake/'+ make +'?format=json';
    # post_fields = {'format': 'json', 'data':'3GNDA13D76S000000;5XYKT3A12CG000000'};
    r = requests.post(url);
    print(r.text);
