from django.db import models
from django.contrib.auth.models import User
from datetime import datetime, date, time
from django.utils.timezone import now


try:
    from rest_framework.authtoken.models import Token
    for user in User.objects.all():
        Token.objects.get_or_create(user=user)

except Exception as e:
     print( '%s (%s)' % (str(e), type(e)))

REVIEW_RATING_CHOICES = (
    ('1', ('Terrible')),
    ('2', ('Poor')),
    ('3', ('Average')),
    ('4', ('Very Good')),
    ('5', ('Excellent')),
)

SMALL=64
MEDIUM=128
LARGE=256
MEGA=512
ORDER_STATES=(('New','New'),('Cancelled','Cancelled'),('Completed','Completed'),('Delivered','Delivered'),('Dispatched','Dispatched'),('Accepted','Accepted'),('Declined','Declined'),('Sent','Sent'))
SHIPPING_MODES=(('Pick Up','Pick Up'),('Delivery','Delivery'))

class BaseModel(models.Model):
    created=models.DateTimeField(auto_now_add=True,null=True)
    modified=models.DateTimeField(auto_now=True,null=True)
    class Meta:
        abstract=True
        ordering = ['-modified']


class AvatarImage(BaseModel):
    image = models.ImageField(blank=True,upload_to='Images/Avatars')



class UserProfile(BaseModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True, related_name='profile')
    avatar = models.OneToOneField(AvatarImage,on_delete=models.CASCADE, null=True, blank=True, related_name='profile')
    isMechanic = models.BooleanField(default=False)
    isSparePartsDealer = models.BooleanField(default=False)
    pushToken = models.CharField(blank=True,default='', max_length=500)
    phone = models.CharField(blank=True,default='', max_length=500)

    def __str__(self):
        return str(self.id) +' - '+self.user.username


class PhoneNumber(BaseModel):
    number = models.CharField(blank=True,default='', max_length=500)
    profile = models.ForeignKey(UserProfile, blank=True, null=True, related_name='phoneNumber', on_delete=models.CASCADE)

    def __str__(self):
        return self.number


class Category(BaseModel):
    name = models.CharField(blank=True,default='', max_length=500)
    slug = models.SlugField(default='', blank=True, null=True)
    image = models.ImageField(blank=True,upload_to='Images/CategoryImages')
    parent = models.ForeignKey('self',blank=True, null=True ,related_name='children', on_delete=models.CASCADE)
    class Meta:
        unique_together = ('slug', 'parent',)    #enforcing that there can not be two
        verbose_name_plural = "categories"

    def __str__(self):
        full_path = [self.name]                  # post.  use __unicode__ in place of
                                                 # __str__ if you are using python 2
        k = self.parent

        while k is not None:
            full_path.append(k.name)
            k = k.parent

        return ' -> '.join(full_path[::-1])





class Condition(BaseModel):
    name = models.CharField(blank=True,default='', max_length=500)

    def __str__(self):
        return self.name

class SparePart(BaseModel):
    name = models.CharField(blank=True,default='', max_length=500)
    description = models.CharField(blank=True,default='', max_length=1500)
    shortDescription = models.CharField(blank=True,default='', max_length=1500)
    price = models.FloatField(default=0)
    isInStock = models.BooleanField(default=True)
    isActive = models.BooleanField(default=True)
    isFeatured = models.BooleanField(default=True)
    isOnSale = models.BooleanField(default=False)
    isDeleted = models.BooleanField(default=False)
    isBelgium = models.BooleanField(default=False)
    salePrice = models.FloatField(default=0)
    categories = models.ManyToManyField(Category, blank=True, )
    seller = models.ForeignKey(UserProfile, default='', on_delete=models.CASCADE, related_name='product')
    condition = models.ForeignKey(Condition, on_delete=models.CASCADE, null=True, blank=True, related_name='product')

    def __str__(self):
        return self.name


class SearchTags(BaseModel):
    tagName = models.CharField(blank=True,default='', max_length=500)
    sparePart = models.ForeignKey(SparePart, blank=True, null=True, related_name='phonenumber', on_delete=models.CASCADE)

    def __str__(self):
        return self.number

class ProductImage(BaseModel):
    name = models.CharField(blank=True,default='', max_length=500)
    oldUrl = models.CharField(blank=True,default='', max_length=500)
    image = models.ImageField(blank=True,upload_to='Images/ProductImages')
    product = models.ForeignKey(SparePart, on_delete=models.CASCADE, blank=False, null=True, related_name="productImages")


class SellerRating(BaseModel):
    starRating = models.IntegerField(null=True)
    seller = models.ForeignKey(UserProfile, on_delete=models.CASCADE, blank=False, null=True, related_name="sellerRating")

class ProductRating(BaseModel):
    starRating = models.IntegerField(null=True)
    product = models.ForeignKey(SparePart, on_delete=models.CASCADE, blank=False, null=True, related_name="productRating")


class Review(BaseModel):
    text = models.CharField(blank=True,default='', max_length=500)
    author = models.ForeignKey(UserProfile, on_delete=models.CASCADE, blank=False, null=True, related_name="review")
    title = models.CharField(blank=True,default='', max_length=500)
    image = models.ImageField(blank=True,upload_to='Images/ProductReviews')
    product = models.ForeignKey(SparePart, on_delete=models.CASCADE, blank=False, null=True, related_name="images")
    sellerRating = models.ForeignKey(SellerRating, on_delete=models.CASCADE, blank=False, null=True, related_name="sellerRating")
    productRating = models.ForeignKey(ProductRating, on_delete=models.CASCADE, blank=False, null=True, related_name="productRating")


class ReviewImage(BaseModel):
    name = models.CharField(blank=True,default='', max_length=500)
    oldUrl = models.CharField(blank=True,default='', max_length=500)
    image = models.ImageField(blank=True,upload_to='Images/ProductImages')
    review = models.ForeignKey(Review, on_delete=models.CASCADE, blank=False, null=True, related_name="reviewImages")



class GeoLocation(BaseModel):
    latitude = models.CharField(blank=True,default='', max_length=500)
    longitude = models.CharField(blank=True,default='', max_length=500)
    approximation = models.CharField(blank=True,default='', max_length=500)
    shop = models.ForeignKey(Review, on_delete=models.CASCADE, blank=False, null=True, related_name="geoLocation")




class Shop(BaseModel):
    logo = models.ImageField(blank=True,upload_to='Images/Logo')
    name = models.CharField(blank=True,default='', max_length=500)
    shopText = models.CharField(blank=True,default='', max_length=1000)
    specialization = models.CharField(blank=True,default='', max_length=250)
    hours = models.CharField(blank=True,default='', max_length=500)
    isActive = models.BooleanField(default=True)
    seller = models.ForeignKey(UserProfile,  default='', null=True,on_delete=models.CASCADE, related_name='shop')

    def __str__(self):
        return self.name


    #TODO: CALCULATE TOTAL SHOP RATINGS IN A MODEL FUNCTION
    def shop_ratings(self):
        totalRatings = self.seller.sellerRating.all()
        cumulative = 0
        for rating in totalRatings:
            cumulative += rating.starRating
        return cumulative/len(totalRatings)


class ShopPhoneNumber(BaseModel):
    number = models.CharField(blank=True,default='', max_length=500)
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE, blank=False, null=True, related_name="shopPhoneNumber")

    def __str__(self):
        return self.number


class Address(BaseModel):
    name = models.CharField(blank=True,default='', max_length=500)
    line1 = models.CharField(blank=True,default='', max_length=500)
    line2 = models.CharField(blank=True,default='', max_length=500)
    state = models.CharField(blank=True,default='', max_length=500)
    landmark = models.CharField(blank=True,default='', max_length=500)
    phoneNumber = models.CharField(blank=True,default='', max_length=500)
    shop = models.ForeignKey(Shop, default='', blank=True, null=True, related_name='address', on_delete=models.CASCADE)
    def __str__(self):
        return self.line1

class CouponCode(BaseModel):
    code = models.CharField(blank=True,null=True, default='', max_length=500)
    amount =models.CharField(max_length=MEDIUM,default='', null=True)
    discount_type =models.CharField(max_length=MEDIUM,default='', null=True)
    seller =models.ForeignKey(UserProfile,  on_delete=models.CASCADE, blank=True, null=True, default='',related_name='coupon')
    date_expires = models.DateField('Expiry Date', default=now)


class ShippingMode(BaseModel):
    mode = models.CharField(default='Pick Up', choices=SHIPPING_MODES,  max_length=500)
    def __str__(self):
        return self.mode

class Order(BaseModel):
    _id = models.CharField(blank=True,null=True, default='', max_length=500)
    address = models.ForeignKey(Address, on_delete=models.CASCADE, blank=True,null=True)
    customer = models.ForeignKey(UserProfile, blank=True, null=True, on_delete=models.CASCADE, related_name='orders')
    status =models.CharField(max_length=SMALL,default='New', null=True, choices=ORDER_STATES)
    comments = models.CharField(default='', max_length=500, blank=True, null=True,)
    shippingMode = models.ForeignKey(ShippingMode,  max_length=500, on_delete=models.CASCADE, blank=True,null=True,related_name='order')
    totalPrice = models.FloatField(default=0, blank=True,null=True,)
    lineItemsTotal = models.FloatField(default=0, blank=True,null=True,)
    shippingCost = models.FloatField(default=0, blank=True,null=True,)
    sentEmail = models.BooleanField(default=False)


class PaymentMethod(BaseModel):
    name = models.CharField(blank=True,null=True, default='', max_length=SMALL)
    order = models.ForeignKey(Order, blank=True, null=True, on_delete=models.CASCADE, related_name='paymentMethod')


class LineItem(BaseModel):
    quantity = models.FloatField(default=0)
    product = models.ForeignKey(SparePart, on_delete=models.CASCADE, blank=True, default='',related_name='line_product')
    order = models.ForeignKey(Order, on_delete=models.CASCADE, blank=True, default='',related_name='line_items')


class CartItem(BaseModel):
    quantity = models.FloatField(default=0)
    product = models.ForeignKey(SparePart, on_delete=models.CASCADE, blank=True, default='',related_name='cartItem')

class Cart(BaseModel):
    cartItems = models.ManyToManyField(CartItem, blank=True, default='',related_name='cart')
    user = models.ForeignKey(UserProfile, on_delete=models.CASCADE, null=True, blank=True, default='',related_name='cart')

class WishList(BaseModel):
    products = models.ManyToManyField(SparePart, blank=True, default='',related_name='wishlist')
    user = models.ForeignKey(UserProfile, on_delete=models.CASCADE, null=True, blank=True, default='',related_name='wishlist')

