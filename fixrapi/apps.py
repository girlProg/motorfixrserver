from django.apps import AppConfig


class FixrapiConfig(AppConfig):
    name = 'fixrapi'
