from django.contrib import admin
import inspect
from django.db import models as django_models
from . import models

for name,obj in inspect.getmembers(models):
    if inspect.isclass(obj) and issubclass(obj,django_models.Model) and obj._meta.abstract is not True and name not in ['User']:
        class Admin(admin.ModelAdmin):
            def get_list_display(self,request):
                l = [t.name for t in self.model._meta.fields if t.editable]
                m = self.model.get_deferred_fields(self.model)
                return list(l)+list(m)
            class Meta:
                model = obj
        if name == "Token":
            pass
        else:
            admin.site.register(obj,Admin)