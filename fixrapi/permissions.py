from rest_framework import permissions
from .models import UserProfile, User


class IsCustomer(permissions.BasePermission):
    def has_permission(self, request, view):
        return UserProfile.objects.filter(user=request.user).count() > 0


class IsSparePartsDealer(permissions.BasePermission):
    def has_permission(self, request, view):
        authenticated_user = UserProfile.objects.filter(user=request.user)
        if authenticated_user and authenticated_user.first().isSparepartsDealer:
            return True
        return False


class IsMechanic(permissions.BasePermission):
    def has_permission(self, request, view):
        if  UserProfile.objects.filter(user=request.user) and UserProfile.objects.filter(user=request.user).first().isMechanic:
            return True

