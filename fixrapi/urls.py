from django.conf import settings
from rest_framework import routers
router = routers.DefaultRouter()
from django.urls import path, include
from django.conf.urls.static import static


from . import views

router.register(r'cart', views.CartViewSet)
router.register(r'cartlineitem', views.CartItemViewSet)
router.register(r'categories', views.CategoryViewSet)
router.register(r'products', views.SparePartViewSet)
router.register(r'userprofile', views.UserProfileViewSet)
router.register(r'shop', views.ShopViewSet)
router.register(r'images', views.ProductImageViewSet)
router.register(r'reviews', views.ReviewViewSet)
router.register(r'sellerratings', views.SellerRatingViewSet)
router.register(r'productratings', views.ProductRatingViewSet)
router.register(r'wishlist', views.WishListViewSet)

urlpatterns = router.urls

#
# urlpatterns = [
#     path(r'^', include(router.urls)),
#
#
# ]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)