from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated
from .models import *
from .serializer import *
from rest_framework import viewsets
from .permissions import IsMechanic, IsSparePartsDealer
from django_filters import rest_framework as filters
from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.response import Response



class UserProfileViewSet(viewsets.ModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer
    # permission_classes = (IsAuthenticated, )


class SparePartViewSet(viewsets.ModelViewSet):
    queryset = SparePart.objects.all()
    serializer_class = ProductSerializer
    filterset_fields = ('categories', 'categories__id')


class ImageViewSet(viewsets.ModelViewSet):
    queryset = ProductImage.objects.all()
    serializer_class = ImageSerializer

class CartItemViewSet(viewsets.ModelViewSet):
    queryset = CartItem.objects.all()
    serializer_class = CartItemSerializer

class CartViewSet(viewsets.ModelViewSet):
    queryset = Cart.objects.all()
    serializer_class = CartSerializer

    def checkCartItems(self, cartitem):
        return cartitem.product.id

    def create(self, request, *args, **kwargs):
        # try:
            productQS = SparePart.objects.filter(id = request.data['id'])
            if len(productQS) > 0:
                #TODO: filtering is done by user so this wont work in production, change the user filter below
                cart, created  = Cart.objects.get_or_create(user__id=1)
                if productQS.first().id in list(map(self.checkCartItems, cart.cartItems.all())) :
                    for cartItem in cart.cartItems.all():
                        if cartItem.product.id == productQS.first().id:
                            cartItem.quantity += 1
                            cartItem.save()
                            return Response({'success' :True, 'cart': CartSerializer(cart, context={'request': request}).data}, status=200)
                            # return Response({'success' :True, "quantity": cartItem.quantity }, status=200)
                else:
                    #if adding new product to the cart
                    cart.cartItems.add(CartItem.objects.create(product=productQS.first(), quantity=1))
                    return Response({'success' :True, 'cart': CartSerializer(cart, context={'request': request}).data}, status=200)

                    # return Response("new product added : "+str(productQS.first().id), status=200)

        # except Exception as e:
        #     # rollbar.report_exc_info(extra_data={'error': e, 'message': e, 'user': request.data })
        #     return Response({'error': 'Error adding to cart: ' + str(e)}, status=409)



class ConditionViewSet(viewsets.ModelViewSet):
    queryset = Condition.objects.all()
    serializer_class = ConditionSerializer

class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

class ShopViewSet(viewsets.ModelViewSet):
    queryset = Shop.objects.all()
    serializer_class = ShopSerializer

class ProductImageViewSet(viewsets.ModelViewSet):
    queryset = ProductImage.objects.all()
    serializer_class = ImageSerializer


class ReviewViewSet(viewsets.ModelViewSet):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    filterset_fields = ('sellerRating__seller',)


class SellerRatingViewSet(viewsets.ModelViewSet):
    queryset = SellerRating.objects.all()
    serializer_class = SellerRatingSerializer


class ProductRatingViewSet(viewsets.ModelViewSet):
    queryset = ProductRating.objects.all()
    serializer_class = ProductRatingSerializer


class WishListViewSet(viewsets.ModelViewSet):
    queryset = WishList.objects.all()
    serializer_class = WishListSerializer
    filterset_fields = ('user__id',)

    def create(self, request, *args, **kwargs):
        try:
            productQS = SparePart.objects.filter(id = request.data['id'])
            if len(productQS) > 0:
                #TODO: need to add user as parameter for wishlist creation
                wl, created  = WishList.objects.get_or_create(user__id=1)
                if len(wl.products.all().filter(pk=productQS.first().pk)) > 0:
                    wl.products.remove(productQS.first())
                    return Response("false"+str(productQS.first().id), status=200)
                else:
                    wl.products.add(productQS.first())
                    return Response("true"+str(productQS.first().id), status=200)
        except ObjectDoesNotExist as e:
            # rollbar.report_exc_info(extra_data={'error': e, 'message': e, 'user': request.data })
            return Response({'error': 'Error changing password: ' + str(e)}, status=409)


