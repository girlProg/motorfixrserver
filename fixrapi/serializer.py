from rest_framework import serializers
from .models import *



class ShopSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shop
        fields = "__all__"
        depth = 2


class UserProfileSerializer(serializers.ModelSerializer):
    shop = ShopSerializer(many=True)
    class Meta:
        model = UserProfile
        fields = "__all__"
        # fields = ('id', 'pushToken', 'avatar', 'shop', 'user', 'isMechanic', 'isSparePartsDealer', 'phone')
        depth = 2

class ImageSerializer(serializers.ModelSerializer):
    #problematic? doesnt let product serializer show
    class Meta:
        model = ProductImage
        fields = ('name', 'url', 'image')
        depth = 3

class ConditionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Condition
        fields = "__all__"
        depth = 2


class ProductSerializer(serializers.ModelSerializer):
    seller = UserProfileSerializer()
    productImages = ImageSerializer(many=True)
    condition = ConditionSerializer()
    class Meta:
        model = SparePart
        fields = "__all__"
        # fields = ('name', 'productImages', 'condition', 'price', 'seller')
        depth = 2

class CartItemSerializer(serializers.ModelSerializer):
    product = ProductSerializer()
    class Meta:
        model = CartItem
        fields = "__all__"
        depth = 2

class CartSerializer(serializers.ModelSerializer):
    cartItems = CartItemSerializer(many=True)
    class Meta:
        model = Cart
        fields =  "__all__"
        depth = 2

class PhoneNumberSerializer(serializers.ModelSerializer):
    class Meta:
        model = PhoneNumber
        fields = "__all__"
        depth = 2


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = "__all__"
        depth = 2


class ReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = "__all__"
        depth = 2

class SellerRatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = SellerRating
        fields = "__all__"
        depth = 2

class ProductRatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductRating
        fields = "__all__"
        depth = 2


class WishListSerializer(serializers.ModelSerializer):
    products = ProductSerializer(many=True)
    class Meta:
        model = WishList
        fields = "__all__"
        depth = 3
